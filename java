@Controller                                           
public class IndexController {                        
                                                      
	@GetMapping("/")                                  
	@ResponseBody                                     
	public String index() {                           
		return "Hello World " + LocalDateTime.now();  
	}                                                 
                                                      
}
